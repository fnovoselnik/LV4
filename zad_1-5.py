# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 11:36:43 2015

@author: Filip
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
#from sklearn.metrics import mean_squared_error
#from sklearn.metrics import r2_score
from sklearn.metrics import *


def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
    
    
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
    
    
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)


plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]


x = x[:, np.newaxis] #dodajemo jos jednu os da bi dobili ispravan ulaz za skilearn
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]


xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]


#plt.figure(2)
#plt.plot(xtrain,ytrain,'ob',label='train')
#plt.plot(xtest,ytest,'or',label='test')
#plt.xlabel('x')
#plt.ylabel('y')
#plt.legend(loc = 4)


linearModel = lm.LinearRegression() #kreiramo novi objekt linearModel iz klase LinearRegression
linearModel.fit(xtrain,ytrain)  #trening podatke provlacimo kroz model

print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'


ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)


plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)


x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)



#zad2


def closed_form(xtrain,ytrain):
    k = np.c_[np.ones(len(xtrain)),xtrain] #dodajemo 1 u prvi stupac x0
    u = np.linalg.inv(np.dot(np.transpose(k),k))
    l = np.dot(np.transpose(k),ytrain)
    thetaML = np.dot(u,l)
    print thetaML
    
#closed_form(xtrain,ytrain)


#Zad 3
  

alpha = 0.05
n_iter = 1000
def gradient_descent(alpha, xtrain, ytrain, n_iter):
    m = xtrain.shape[0]
    theta = np.zeros(2)
    xtemp = np.c_[np.ones(len(xtrain)),xtrain]
    x_transpose = xtemp.transpose()
    J = np.zeros((n_iter,1))
    for i in range(0, n_iter):
        h = np.dot(xtemp,theta)
        h = h[:,np.newaxis]
        loss = h - ytrain
        J[i] = np.sum(loss**2) / (2*m)  
        grad = np.dot(x_transpose, loss) / m 
    
        theta[0] = theta[0] - (alpha*grad[0])
        theta[1] = theta[1] - (alpha*grad[1])
        
    return theta
    
    plt.figure(4)
    plt.plot(range(n_iter),J)
    
    plt.figure(5)
    for i in range(xtemp.shape[1]):
        y_predict = theta[0] + theta[1]*xtemp
    plt.plot(xtemp[:,1],ytrain,'o')
    plt.plot(xtemp,y_predict,'k-')
    plt.show()
    

#print 'Parametri theta - zatvorenoa forma: ', closed_form(xtrain,ytrain)
#print 'Parametri theta - gradijentni spust: ', gradient_descent(alpha, xtrain, ytrain, n_iter)

thetas = gradient_descent(alpha, xtrain, ytrain, n_iter)
print thetas

#zad4

def MSE(x1,y1):
    n = x1.shape[0]
    xtemp = np.c_[np.ones(len(x1)),x1]
    h = np.dot(xtemp,thetas)
    h = h[:,np.newaxis]
    loss = h - y1
    MSE_func = np.sum(loss**2) / n
    print 'Mean squared eror - function: ', MSE_func
    
    y1_p = linearModel.predict(x1)
    MSE_scikit = mean_squared_error(y1, y1_p)
    print 'Mean squared eror - scikit: ', MSE_scikit
    
#MSE(xtest,ytest) # kod pozivanja funkcije predajemo train ili test podatke ovisi sto zelimo
  
  

def R2_score(x1,y1):
    n = x1.shape[0]
    xtemp = np.c_[np.ones(len(x1)),x1]
    h = np.dot(xtemp,thetas)
    h = h[:,np.newaxis]
    loss = y1 - h
    y_num = np.sum(loss**2)
    y_sum = np.sum(y1) / n
    y_den = np.sum((y1-y_sum)**2)
    R2score = 1 - (y_num/y_den)
    print 'R2 score - function: ', R2score
    
    y2_p = linearModel.predict(x1)
    R2score_scikit = r2_score(y1, y2_p)
    print 'R2 score - scikit: ', R2score_scikit
    
#R2_score(xtrain,ytrain) # kod pozivanja funkcije predajemo train ili test podatke ovisi sto zelimo

#zad5

xtemp = np.c_[np.ones(len(xtest)),xtest]
h = np.dot(xtemp,thetas)
h = h[:,np.newaxis]
e = ytest - h
    
plt.figure(6)
plt.scatter(xtest,e) 
    











