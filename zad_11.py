# -*- coding: utf-8 -*-
"""
Created on Sun Dec 20 18:01:34 2015

@author: Filip
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import pandas as pd
from sklearn.datasets import load_boston
import sklearn


#Eksplorativna analiza


boston = load_boston()

data = pd.DataFrame(boston.data)
data.columns = boston.feature_names


print boston.target[:5]
data['PRICE'] = boston.target

print data

x = data.drop('PRICE', axis = 1)
linearModel = lm.LinearRegression()
print linearModel.fit(x,data.PRICE)

#print 'Estimated coeff: ',linearModel.coef_
#print 'Estimated intercept: ', linearModel.intercept_
#print 'Number of coeff: ' ,len(linearModel.coef_)

plt.figure(1)
plt.scatter(data.RM,data.PRICE)
plt.xlabel('Average number of rooms per dwelling (RM)')
plt.ylabel('Housing price')
plt.title('RM vs. PRICE')

print linearModel.predict(x)[0:7]

plt.figure(2)
plt.scatter(data.PRICE, linearModel.predict(x))
plt.xlabel('Prices')
plt.ylabel('Predicted prices')
plt.title('Prices vs. Predicted prices')

MSE = np.mean((data.PRICE - linearModel.predict(x))**2)
print MSE

x_train, y_train, x_test, y_test = sklearn.cross_validation.train_test_split(x, data.PRICE, test_size = 0.33, random_state = 5)

print x_train.shape
print y_train.shape
print x_test.shape
print y_test.shape

#linearModel.fit(x_train, y_train)
#pred_train = linearModel.predict(x_train)
#pred_test = linearModel.predict(x_test)



